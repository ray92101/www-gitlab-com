---
layout: handbook-page-toc
title: Market Insights
---

## Market Insights
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Market Insights at GitLab

Market Insights is a function of the Market Strategy & Insights team in Portfolio Marketing. 

_What is the Market Insights Program?_ 

We are a business partner who:
- Provides deeper market knowledge and understanding, clarity through fresh perspectives, and recommendations for effective business decision-making (innovation initiatives, pricing, messaging, benchmarking, etc.)
- Interprets data in narrative form that adds value and/or incites specific action
- Uses a wide range of data streams and a multi-disciplinary approach to identify near and long-term (1-3 years) growth and innovation strategies as well as a clear path to implementation
- Constantly searches for the “Why/What next?” for the company

*Questions We Seek to Answer:*

- What business-level objectives do enterprise customers need to satisfy? How does this translate into desired product functionality?
- What is the next industry analyst category we need to create/influence? What is the next big thing after the DevOps Platform Era?
- What do enterprise customers really value? Are there areas beyond DevOps where we can play?
- What might enterprise customers want in the future? How are market needs/trends evolving?
- To what extent are customers getting this need fulfilled from GitLab? Do we currently deliver it? (Is it on the roadmap? Should we add it?)
- How can we expand enterprise customer usage of GitLab?
- What adjacent audiences/markets can we identify/address?
- Which levers can we pull to improve product performance and perception relative to competitors?

## Mission of the Program

_To identify, qualify, and activate new avenues of pursuit and profitability for GitLab (range: 1-3 years)_

Our big picture: 

**ELEVATE:** We seek to grow our value proposition beyond benefits/features--or even value drivers + use cases--to enable our customers’ business outcomes  

**IDENTIFY & ARTICULATE:** We look to leverage research & insights to identify [blue ocean strategy](https://about.gitlab.com/company/strategy/) opportunities

**OPERATE:** We will function as the company’s think tank, helping GitLab to become our customers’ recognized and essential catalyst for success

## How Our Work Supports the Organization 

Deliverables that feed into and/or support the needs of other GitLab functions:

- New market identification & use case expansion / Blue Ocean Strategy thinking
- Industry vertical identification, prioritization & planning for implementation
- Research (support primary/ lead secondary) on current and future Enterprise IT trends
- Thought leadership speaking & content (whitepapers, web pages, case studies, conference presentations, sales enablement sessions, and executive presentations)
- Determining new opportunities among existing clients, and participating in new business initiatives
- Collaboration with GTM leaders in strategic planning and investment efforts
- Industry analyst briefing participation & inputs
- Regular Market Insights Team briefings

_How can Market Insights benefit your team?_

Market Insights deliverables can be used to (and/or augment your efforts to):

- Find new revenue sources
- Ensure messaging is reaching the precise customers for which it was tailored
- Develop marketing campaigns and pricing strategies
- Measure current performance against market potential
- Monitor brand awareness and customer sentiment
- Learn how our target customers actually use GitLab  on a daily basis, as well as their thought process during the buying process
- Understand what draws businesses to purchase from our competitors
- Create customized interactions or predict future actions using previous buying patterns or behaviors


## Market Insights Responsibilities

- Conduct research on current and future Enterprise IT trends through written reports, surveys, interviews and interactions with market analysts, industry conferences, self-study, and interactions with our customers to develop key insights for GitLab
- Help GitLab develop a deep knowledge and understanding of enterprise buyers including their challenges and goals that help them drive success for their companies
- Assess the DevOps ecosystem to distill the trends that influence GitLab’s addressable market
- Contribute to a consumable deliverable (in-person discussion, presentation, written report) 
- Evaluate, analyze and present research results in a consumable and actionable format (whitepapers, web pages, case studies, conference presentations, sales enablement sessions, and executive presentations)
- Help build and grow GitLab as a thought leader in the DevOps space by translating market insights into actionable recommendations for Product investment and GTM
- Participate and help prepare for industry analyst briefings as required
- Collaborate with GTM leaders (e.g. Sales, Channel, Sales Ops & Strategy) in strategic planning and investment efforts

## The Market Insights Process & Potential Market Approaches

**METHODOLOGY**
1. **Research & Data Gathering:** Collect information and data points, conduct and lead research, and build internal and external relationships to determine priorities and support the program.
2. **Analysis:** Synthesize and correlate data to develop fresh perspectives and innovative concepts.
3. **Validation & Iteration:** Incorporate feedback into insight, recommendations, and deliverables. Adjust research direction as necessary.
4. **Develop & Present Deliverables:** Translate insights and recommendations into consumable publications.
5. **Create Insights & Recommendations:** Distill insight into actionable ideas.

<br>

**RESEARCH PROCESS:**

1. **Topic identification** (internally decided or by stakeholder request)
2. **Determine required business objective**
   - Research--primary or secondary (technology/market/trend/topic/industry/vertical/solution or use case, user or persona, etc.)
   - Data review & interpretation with recommendations
   - Customer/prospect call participation
   - Event speaking  
   - Thought leadership content creation/review
   - Other 
3. **Articulate the WHY this deliverable is needed (problem statement).** What is currently happening? What is the pain that is being experienced and who is experiencing it? Why now? What will happen if the status quo continues?
   - What is your business objective and underlying rationale for this research?
   - Define the questions that must be answered 

Alternatively, is there an emerging technology or solution that we need to explore? What could the intersection of the tech/solution and the GitLab proposition be? Is this tech/solution being used (or could it be used) by our current customers or by potential, non-traditional GitLab users? 

4. **Create the hypothesis** - Develop an explanation, argument, or analytical supposition about the topic. 
5. **Create the appropriate issue(s)/epic(s) and link to source materials and related issues.**
6. **Data gathering from appropriate sources**: Collect information and data points, conduct and lead research. Sources include but are not limited to:
   - Primary or secondary research (e.g. IDC or Forrester we commission)
   - Social media and SEM analytics (e.g. [HackerNews](https://news.ycombinator.com/), LinkedIn, internal reports from SEO/SEM team)
   - Mainstream media and industry articles / reporting (Wall Street Journal, Washington Post, New York Times, [BBC News](https://www.bbc.com/news), [DevOps.com](https://devops.com/category/blogs/), Government Executive Media Group publications, etc.)
   - Market growth statistics (from industry analysts, DevOps media sources)
   - Channel / partner input
   - Internal Sales trends & reports
   - Sales feedback and QBRs
   - Customer meetings, calls & Chorus, SFDC
   - Analyst publications, conversations & presentations (e.g. Gartner, Forrester, IDC, 451 Research, etc.)
   - Competitive intelligence reports
   - CAB (product & C-level) feedback
   - Peer review comments (e.g. [G2](https://www.g2.com/products/gitlab/reviews#survey-response-5271767))
   - Wider future market trends and tech & business-focused reports from industry leaders (e.g. Accenture, Deloitte, McKinsey, HBR, etc.)
   - Industry conferences
   - Internal collaborative issues / topic & research requests
(Please share any interesting data sources, articles, etc. we'd find helpful in the _#market-insights_ Slack channel.)

7. **Analyze data:** Synthesize and correlate data to develop fresh perspectives and innovative concepts
   - Use tools from the Market Insights Tool Chest to process and analyze data, current conditions, forecasts, trends, etc. to develop strategic recommendations.  
8. **Validate & Iterate:** Incorporate feedback into insight, recommendations, and deliverables. 
Adjust research direction as necessary.
9. **Create Insights & Recommendations:** Distill insight into actionable ideas and suggested activities. 
10. **Develop & Present Deliverables:** Translate insights and recommendations into consumable publications. Specific deliverables may include:
   - An [Innovation Play](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/6026)
   - A business case/rationale for investment or course of action
   - A report or summary assessment
   - A discussion
   - A presentation, whitepaper, case study, sales enablement, or executive presentation
   - A speaking engagement
   - An analyst briefing


**PROCESS FOR MARKET OBSERVATION OF INDUSTRY TRENDS & EVENTS:**

1. Identify an event (virtual, webinar, in-person) for attendance or an industry deliverable to consume.
2. Register and attend or read/watch content piece.
3. Take notes with appropriate screenshots. Insert links to relevant related content/sites. 
4. Add GitLab-relevant insights and recommendations. 
5. Download slides and/or deliverable to the Market Insights REPORTS & PUBS folder and post the notes in the NOTES folder. Make sure to link the replay and any login credentials in the top section of the notes document. 
6. Highlight any essential learnings on the Monthly Bulletin and inform stakeholders in the _# market-insights_ slack channel of any action items.





**GENERAL PROCESS FOR INNOVATION ACTIVATION:**

_What market approaches might GitLab take based on the identified opportunities?_

After a topic has been researched (see "RESEARCH PROCESS") and a recommendation for the business has been identified, Market Insights will help the DRI and team with next steps for activating the recommendation. (See "POTENTIAL MARKET APPROACHES".)  

Recommendations for potential market approaches based on the results of the data assessed fall on the following spectrum of activity, ranging from the easiest level of effort, _Solution or Use Case Development_, to _Innovation Play/Campaign_ to _Roadmap/Feature Development_ to _Category Creation_ to finally the most difficult and investment-intensive, _Market/Industry Repositioning_.

!![Market_Insights_Function__Objectives___Plan](/uploads/557c01857e171b62bc21ad4f6c38267b/Market_Insights_Function__Objectives___Plan.png)

Based on the most appropriate approach for the opportunity, Market Insights will provide activation assistance to ensure that recommendation activities are sufficiently ramped up and owned by the DRI/ stakeholder(s). 

In certain cases, this may entail creating or supporting the development of a full business case for E-Group presentation and approvals. In other cases, some recommendations may initially be best activated in an experimental manner, allowing data to be collected that can serve as real-world proof points for further analysis and recommendation.


Based on business priorities and constraints including executive direction, level of effort, cost, time, and other factors, we may choose to:
- Create a new solution/use case 
- Develop an inovation play or campaign
- Add a feature request to the roadmap
- Pursue creating a new category
- Reposition ourselves in the market





## How to Engage with Market Insights

- Please join the discussion in the [#market-insights](https://gitlab.slack.com/archives/CHXUPL2EP) channel. 

- View topics we are researching on our [Market Insight Research Board](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/boards/3593018?label_name[]=Market%20Insights) 
- To create a Market Insights topic request, ask for insight into data or a report, make a request for us to join a customer call, or suggest a topic for research, please create an issue using the [Market Insights Request Template](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/new#) in the Product Marketing project.


## Market Insights OKRs
[FY22 Q4 OKR: Enhance the Market Insights/Strategy Program to bring more Awareness to GitLab Product Positioning](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/5558)

1. [FY23 Q1 Market Insights OKR: Market Insights Thought Leadership Message House](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/5882)
2. [FY23 Q1 Market Insights OKR: Market Insights Thought Leadership Deliverables](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/5828)
3. [FY23 Q1 Market Insights OKR: Market Research Strategy for Channel & Alliances](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/5827)
4. [FY23 Q1 OKR: Market Insights: Market Opportunity Business Case](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/5910)


## How We Measure Our Results
- Number of Product Roadmap issues and/or comments and product direction impact (narrative-driven)
- [KCG Share of Voice measurement metric](https://docs.google.com/presentation/d/1OVEZOAG8O5veRws6QE8dyjbTmyQ9LE3x/edit#slide=id.p19) targets increase 20% (baseline measure) 


## Resources 

- See our library of [research notes](https://drive.google.com/drive/folders/1PIUKOJCYPWPvNa-2h_r91Dz-i-4KCNSu?usp=sharing) and [recommendations](https://drive.google.com/drive/folders/1TXJrXleg2sB8qhIa5sAyqlqVAK_hljeI?usp=sharing) 

- View our library of source [presentations and reports](https://drive.google.com/drive/folders/12QE97kGOfsIFbKqMMrBbwdRfew2IfyZM?usp=sharing)


##Market Insights Team
- [Traci](https://about.gitlab.com/company/team/#tracirobinson), Manager, Market Insights
- [Laura](https://about.gitlab.com/company/team/#lclymer), Director, Market Strategy & Insights

