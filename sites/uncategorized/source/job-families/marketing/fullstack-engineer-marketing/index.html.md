---
layout: job_family_page
title: Fullstack Engineer - Marketing
---

## Levels

### Fullstack Engineer (Intermediate) 

The Fullstack Engineer (Intermediate) reports to the [Senior Manager, Digital Experience](/job-families/marketing/marketing-web-developer-designer/#senior-manager-digital-experience).

#### Fullstack Engineer (Intermediate) Job Grade

The Fullstack Engineer (Intermediate) is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Fullstack Engineer (Intermediate) Responsibilities

* Implement frameworks, code style guides, and templates to empower everyone to contribute.
* Implement site speed improvements and technical SEO.
* Support the architecture and engineering (HTML, CSS, JS, Ruby, Middleman, Haml) of about.gitlab.com.
* Assist in building and maintaining our Content Management System.
* Run the marketing website as an open source project, optimizing for maximum contributions to both code and content.

#### Fullstack Engineer (Intermediate) Requirements

* 3-5 years experience specializing in full stack development, website and web applications.
* Expert knowledge of HTML, CSS, HAML and JavaScript (jQuery, Vue.js).
* Understanding of responsive design and best practices.
* The ability to iterate quickly and embrace feedback from many perspectives.
* Knowledge of information architecture, interaction design, and user-centered design.
* Knowledge of Git and comfortability using the command line.
* Experience with Jamstack, Ruby, and Middleman (and/or other static site generators).
* Ability to use GitLab.
* Previous experience with Static Site Generators like Middleman, Jekyll, Hugo, etc., preferred. 
* Experience working in a fully or partially remote company, preferred. 
* A positive outlook on changing priorities, preferred. 
* The ability to proactively question and improve priorities, preferred.
* Marketing engineers should be familiar with marketing concepts such as conversion, analytics, A/B testing, lead generation, buyer journeys, and search engine optimization.

### Senior Fullstack Engineer

The Senior Fullstack Engineer reports to the [Senior Manager, Digital Experience](/job-families/marketing/marketing-web-developer-designer/#senior-manager-digital-experience).

#### Senior Fullstack Engineer Job Grade

The Senior Fullstack Engineer is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades). 

#### Senior Fullstack Engineer Responsibilities

* Extends the Fullstack Engineer (Intermediate) responsibilities.
* Lead the architecture and engineering of about.gitlab.com. 
* Research and define frameworks, code style guides, and templates to empower everyone to contribute.
* Work with cross functional partners, as a team leader. 
* Critical decision making, and knowing what will have the greatest business impact when prioritizing. 
* Own CI automation using GitLab to build, test, and deploy.
* Sharing knowledge and educating on best practices and new technologies.

#### Senior Fullstack Engineer Requirements

* Extends that of the Fullstack Engineer (Intermediate) requirements.
* 6+ years experience specializing in fullstack development, website and web applications.
* Uses research, data, and best practices to create, validate, and present ideas to key
stakeholders.
* A track record of being self-motivated, results oriented, and delivering on time.
* Expert in selecting and applying frameworks/systems to solve complicated technical problems. 
* The ability to communicate complex ideas and solutions to non-technical stakeholders. 

## Performance Indicators

* [Contributing to the success of Marketing's Quarterly Initiatives](https://about.gitlab.com/handbook/marketing/inbound-marketing/#q3-fy21-initiatives)
* [Identifying and organizing epics into executable Sprint plans](https://about.gitlab.com/handbook/marketing/growth-marketing/brand-and-digital-design/#sprint-planning)
* [Successfully completing weekly Sprint tasks](https://about.gitlab.com/handbook/marketing/growth-marketing/brand-and-digital-design/#sprint-cycle)
* [Collaborating on identifying issues to be completed within Epics](https://about.gitlab.com/handbook/marketing/inbound-marketing/#epics)

## Career Ladder

The next step in the Website Full Stack Development job family is not yet defined at GitLab. 

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, you can find their job title on our [team page](/company/team/).

* Select candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/#screening-call) with one of our Global Recruiters.
* Next, candidates will be invited to schedule a 30 minute interview with our Senior Manager, Digital Experience. 
* Next, candidates will be invited to schedule a 30 minute technical interview with our Fullstack Engineer and an interview with a Senior Brand Designer. 
* Next, candidates will be invited to schedule a 30 minute interview with the Senior Director, Growth Marketing. 
* Finally, candidates will be invited to schedule a 30 minute follow up interview with the Senior Manager, Digital Experience. 
* Successful candidates will subsequently be made an offer via phone or video. 

Additional details about our process can be found on our [hiring page](/handbook/hiring/).
